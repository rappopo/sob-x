const { _ } = require('rappopo-sob').Helper
const Gpsd = require('node-gpsd-client')

module.exports = ({ sob }) => {
  const config = sob.config
  return {
    started () {
      this.clients = {}
      this._connect()
    },
    stopped () {
      _.forOwn(this.clients, (v, k) => {
        // v.close()
        this.logger.info(`Instance '${k}' closed`)
      })
    },
    actions: {
      connect (ctx) {
        return this._connect(ctx.params.name)
      },
      get (ctx) {
        return this._get(ctx.params.name)
      },
      list (ctx) {
        return this._list()
      }
    },
    methods: {
      _connect (name) {
        if (_.isEmpty(name)) name = 'default'
        const client = new Gpsd(config.gpsd[name])
        this.clients[name] = client
        client.instanceName = name
        client.on('connected', () => {
          this.broker.broadcast(`x:gpsd:connect:${client.instanceName}`, client)
        })
        client.on('error', err => {
          this.broker.broadcast(`x:gpsd:error:${client.instanceName}`, err)
        })
        client.on('disconnected', () => {
          this.broker.broadcast(`x:gpsd:disconnect:${client.instanceName}`)
        })
        client.on('reconnecting', () => {
          this.broker.broadcast(`x:gpsd:reconnect:${client.instanceName}`)
        })
        client.connect()
      },
      _get (name) {
        if (_.isEmpty(name)) name = 'default'
        if (!this.clients[name]) throw new Error(`Unknown '${name}' gpsd instance`)
        return this.clients[name]
      },
      _list () {
        return this.clients
      }
    }
  }
}
