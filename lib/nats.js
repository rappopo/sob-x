const { _ } = require('rappopo-sob').Helper
const { connect, Payload } = require('ts-nats')

/*
auto subscribe:
...
{ settings: {
    xNatsSubscribe: 'xxx.yyy.zzz'            <- 'default' instance
    xNatsSubscribe: ['instance:xxx.yyy.zzz'] <- custom instance
  }
}
*/

module.exports = ({ sob }) => {
  const config = sob.config
  return {
    async started () {
      this.ncs = {}
      this.subs = []
      for (const n in config.nats) {
        await this._connect(n)
      }
    },
    stopped () {
      _.forOwn(this.ncs, (v, k) => {
        v.close()
        this.logger.info(`Instance '${k}' closed`)
      })
    },
    actions: {
      connect (ctx) {
        return this._connect(ctx.params.name)
      },
      get (ctx) {
        return this._get(ctx.params.name)
      },
      list (ctx) {
        return this._list()
      },
      subscribe (ctx) {
        return this._subscribe(ctx.params.topic, ctx.params.name, ctx.params.options)
      },
      unsubscribe (ctx) {
        return this._unsubscribe(ctx.params.sid, ctx.params.name)
      },
      publish (ctx) {
        return this._publish(ctx.params.topic, ctx.params.payload, ctx.params.name, ctx.params.reply)
      },
      subs (ctx) {
        return this.subs
      }
    },
    methods: {
      _connect (name) {
        config.nats[name].payload = Payload.JSON
        if (_.isEmpty(name)) name = 'default'
        return new Promise((resolve, reject) => {
          let nc
          connect(config.nats[name]).then(result => {
            nc = result
            this.ncs[name] = nc
            nc.sobNats = name
            nc.on('error', err => {
              this.logger.error(`'${name}': ${err.message}`)
              this.broker.broadcast(`x:nats:error:${name}`, err)
            })
            nc.on('connect', async () => {
              this.logger.info(`'${nc.sobNats}' connected`)
              this.broker.broadcast(`x:nats:connect:${nc.sobNats}`)
              await this.applySubscriber()
            })
            nc.on('disconnect', () => {
              this.logger.warn(`'${nc.sobNats}' disconnected`)
              this.broker.broadcast(`x:nats:disconnect:${nc.sobNats}`)
            })
            nc.on('close', () => {
              this.logger.info(`'${nc.sobNats}' closed`)
              this.broker.broadcast(`x:nats:close:${nc.sobNats}`)
            })
            nc.on('reconnecting', () => {
              this.logger.warn(`'${nc.sobNats}' reconnecting...`)
              this.broker.broadcast(`x:nats:reconnecting:${nc.sobNats}`)
            })
            nc.on('reconnect', () => {
              this.logger.info(`'${nc.sobNats}' reconnected`)
              this.broker.broadcast(`x:nats:reconnect:${nc.sobNats}`)
            })
            resolve()
          }).catch(err => {
            this.logger.error(`'${name}': ${err.message}`)
            this.broker.broadcast(`x:nats:error:${name}`, err)
            if (err.message.includes('ECONNREFUSED')) {
              setTimeout(() => {
                this._connect(name)
              }, 5000)
            }
            resolve()
          })
        })
      },
      _get (name) {
        if (_.isEmpty(name)) name = 'default'
        if (!this.ncs[name]) throw new Error(`Unknown '${name}' nats instance`)
        return this.ncs[name]
      },
      _list () {
        return this.ncs
      },
      async _subscribe (topic, name, options) {
        if (_.isEmpty(name)) name = 'default'
        const nc = this._get(name)
        const sub = await nc.subscribe(topic, (err, msg) => {
          if (err) {
            this.logger.error(`${name}:${topic} -> ${err.message}`)
          } else {
            this.broker.broadcast(`x:nats:message:${name}:${topic}`, msg.data)
          }
        })
        return sub
      },
      _unsubscribe (topic, name) {
        const idx = _.findIndex(this.subs, { instance: name, topic })
        if (idx === -1) throw new Error(`Subscription not found`)
        this.subs[idx].id.unsubscribe()
        _.pullAt(this.subs, idx)
      },
      _publish (topic, payload, name) {
        if (_.isEmpty(name)) name = 'default'
        const nc = this._get(name)
        nc.publish(topic, payload)
      },
      async applySubscriber () {
        for (let i = 0; i < this.broker.services.length; i++) {
          const s = this.broker.services[i]
          let topics = _.get(s, 'settings.xNatsSubscribe', [])
          if (_.isEmpty(topics)) continue
          if (_.isString(topics)) topics = [topics]
          for (let j = 0; j < topics.length; j++) {
            let t = topics[j]
            let instance = 'default'
            t = t.split(':')
            if (t.length > 1) {
              instance = t[0]
              t.shift()
              t = t.join(':')
            } else {
              t = t[0]
            }
            this.logger.info(`'${s.name}' subscribes to '${instance}:${t}' `)
            await this._subscribe(t, instance)
          }
        }
      }
    }
  }
}
